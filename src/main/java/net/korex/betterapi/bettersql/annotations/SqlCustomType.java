package net.korex.betterapi.bettersql.annotations;

import net.korex.betterapi.bettersql.SqlColumnType;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Target({ElementType.FIELD})
@Retention(RetentionPolicy.RUNTIME)
public @interface SqlCustomType {
    SqlColumnType columnType();
}

