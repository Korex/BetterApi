package net.korex.betterapi.bettersql;

import net.korex.betterapi.bettersql.annotations.SqlAutoIncrement;
import net.korex.betterapi.bettersql.annotations.SqlCustomType;
import net.korex.betterapi.bettersql.annotations.SqlLength;
import net.korex.betterapi.bettersql.annotations.SqlPrimary;

public class Test {

    public static void main(String[] args) {
        SqlManager.getInstance().connect("localhost", "root", "password", "private");

        long l = System.currentTimeMillis();
        Test test = new Test();
        TestData testData = test.get("Kevin Schade");
        testData.setAge(24);


        test.put(testData);

        System.out.println(
                test.get("Kevin Schade"));

        l = System.currentTimeMillis() - l;
        System.out.println(l);
    }

    private AutoSqlTable autoSqlTable;


    public Test() {
        this.autoSqlTable = new AutoSqlTable("AUTO_TABLE_TEST", TestData.class);
        this.autoSqlTable.create();
    }

    public void put(TestData testData) {
        this.autoSqlTable.put(testData).sync();
    }

    public TestData get(String name) {
        return this.autoSqlTable.get("name=" + name).sync().get(0);
    }

}

class TestData {

    @SqlPrimary
    @SqlAutoIncrement
    private long id;
    @SqlLength(length = 64)
    @SqlCustomType(columnType = SqlColumnType.VARCHAR)
    private String name;
    private int age;

    public TestData(String name, int age) {
        this.name = name;
        this.age = age;
    }

    public TestData() {

    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("TestData{");
        sb.append("id=").append(id);
        sb.append(", name='").append(name).append('\'');
        sb.append(", age=").append(age);
        sb.append('}');
        return sb.toString();
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }
}
