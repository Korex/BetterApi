package net.korex.betterapi.bettersql;

import com.sun.org.apache.xpath.internal.operations.Bool;

/**
 * Copyright (c) 2017-2017 district-ronin.de
 * All rights reserved.
 * <p>
 * Represents the different SQL data-types which are registered here.
 * Methods are unimportant for user.
 *
 * @author Korex
 * @version 1.0.0
 * @since 1.0.0
 */
public enum SqlColumnType {
    VARCHAR(""),
    CHAR(""),
    TEXT(""),
    INT(0),
    DOUBLE(0d),
    FLOAT(0f),
    BIGINT(0),
    BOOLEAN(0);

    private Object standart;

    SqlColumnType(Object standart) {
        this.standart = standart;
    }

    public String getNameForQry() {
        return this.toString();
    }

    public Object getStandart() {
        return this.standart;
    }

    public static SqlColumnType fromType(Class<?> clazz) {
        if (clazz == String.class) {
            return SqlColumnType.TEXT;
        }

        if (clazz == Integer.class || clazz == int.class) {
            return SqlColumnType.INT;
        }

        if (clazz == Long.class || clazz == long.class) {
            return SqlColumnType.BIGINT;
        }

        if (clazz == Boolean.class || clazz == boolean.class) {
            return SqlColumnType.BOOLEAN;
        }

        if (clazz == Double.class || clazz == double.class) {
            return SqlColumnType.DOUBLE;
        }

        if (clazz == Float.class || clazz == float.class) {
            return SqlColumnType.FLOAT;
        }

        return null;
    }


}
