package net.korex.betterapi.bettersql.actions;

import net.korex.betterapi.bettersql.SqlResultSet;
import net.korex.betterapi.bettersql.SqlTable;

public class SQLTableContainsAction extends SqlAction<Boolean> {

    private String value;

    public SQLTableContainsAction(SqlTable table, String value) {
        super(table);
        this.value = value;
    }

    public Boolean action() {
        SqlResultSet set = this.getWhereSQLStatement("SELECT * FROM " + this.getTableName(), this.value).query();
        return set.size() >= 1;
    }
}
