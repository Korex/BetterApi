package net.korex.betterapi.bettersql.actions;

import net.korex.betterapi.bettersql.SqlElement;
import net.korex.betterapi.bettersql.SqlStatement;
import net.korex.betterapi.bettersql.SqlTable;
import net.korex.betterapi.bettersql.SqlValueInterpreter;

import java.util.LinkedHashMap;

public class SQLTableUpdateAction extends SqlAction<Void> {

    private String where, what;

    public SQLTableUpdateAction(SqlTable table, String where, String what) {
        super(table);
        this.what = what;
        this.where = where;
    }

    public Void action() {
        LinkedHashMap<String, SqlElement<?>> whatMap = SqlValueInterpreter.getMap(this.what, this.getTable());
        LinkedHashMap<String, SqlElement<?>> whereMap = SqlValueInterpreter.getMap(this.where, this.getTable());
        StringBuilder qry = new StringBuilder();
        qry.append("UPDATE " + super.getTableName() + " SET");

        int index = 0;
        for (String key : whatMap.keySet()) {
            qry.append(" " + key + " = ?");
            if (index != (whatMap.keySet().size() - 1)) {
                qry.append(",");
            }
            index++;
        }

        qry.append(" WHERE");
        index = 0;
        for (String key : whereMap.keySet()) {
            qry.append(" " + key + " = ?");
            if (index != (whereMap.keySet().size() - 1)) {
                qry.append(" AND");
            }
            index++;
        }

        //transfer all data to whatmap
        for (String s : whereMap.keySet()) {
            whatMap.put(s, whereMap.get(s));
        }

        SqlStatement stmt = this.getSQLStatement(qry.toString(), whatMap);
        stmt.update();
        return null;
    }
}
