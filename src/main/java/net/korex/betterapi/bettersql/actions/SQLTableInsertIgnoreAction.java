package net.korex.betterapi.bettersql.actions;

import net.korex.betterapi.bettersql.SqlElement;
import net.korex.betterapi.bettersql.SqlStatement;
import net.korex.betterapi.bettersql.SqlTable;
import net.korex.betterapi.bettersql.SqlValueInterpreter;

import java.util.HashMap;

public class SQLTableInsertIgnoreAction extends SqlAction<Void> {

    private HashMap<String, SqlElement<?>> map;

    public SQLTableInsertIgnoreAction(SqlTable table, String content) {
        super(table);
        this.map = SqlValueInterpreter.getMap(content,this.getTable());
    }

    public Void action() {
        StringBuilder columnsBuidler = new StringBuilder();
        {
            int i = 0;
            for (String key : map.keySet()) {
                i++;
                if (i == this.map.size()) {
                    columnsBuidler.append(key);
                } else {
                    columnsBuidler.append(key + ", ");
                }
            }
        }

        StringBuilder valuesBuilder = new StringBuilder();

        {
            for (int i = 0; i < this.map.keySet().size(); i++) {
                if (i == this.map.size() - 1) {
                    valuesBuilder.append("?");
                } else {
                    valuesBuilder.append("?,");
                }
            }
        }


        String qry = "INSERT IGNORE INTO " + super.getTableName() + " ("
                + columnsBuidler.toString() + ") VALUES (" + valuesBuilder.toString() + ")";
        SqlStatement stmt = new SqlStatement(qry, this.getTable());

        {
            int i = 0;
            for (String key : this.map.keySet()) {
                SqlElement<?> element = this.map.get(key);
                element.setToStatement(i, stmt);
                i++;
            }
        }

        stmt.update();
        return null;
    }
}
