package net.korex.betterapi.bettercommand;

import java.util.List;

public interface BetterCommand {

    String getName();

    List<CommandArgument> getArguments();

    String getDescription();

    boolean requiresPermission();

    List<String> getAliases();

    boolean onlyForPlayer();

}
