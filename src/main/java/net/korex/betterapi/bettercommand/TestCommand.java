package net.korex.betterapi.bettercommand;

import org.bukkit.GameMode;
import org.bukkit.entity.Player;

import java.util.Arrays;
import java.util.List;

public class TestCommand implements BetterCommand {

    @Override
    public List<CommandArgument> getArguments() {
        return Arrays.asList(
                new CommandArgument("creative", this.getRunnableCreative(), "Setzt dich in den Creative Modus").setOnlyForPlayer(),
                new CommandArgument(this.getRunnableGMOthers(), "Setzt den GM eines Spielers", CommandVariable.INTEGER, CommandVariable.PLAYER),
                new CommandArgument(this.getRunnableGMOwn(), "Setzt deinen GM", CommandVariable.INTEGER).setOnlyForPlayer()
        );
    }

    private CommandRunnable getRunnableCreative() {
        return (sender, player, parsedVariables) -> {
            player.setGameMode(GameMode.CREATIVE);
            player.sendMessage("§aDu bist jz im GM 1!");
        };
    }

    private CommandRunnable getRunnableGMOwn() {
        return (sender, player, parsedVariables) -> {
            int number = (Integer) parsedVariables[0];
            player.setGameMode(GameMode.getByValue(number));
            player.sendMessage("§CDu hast deinen GM geändert!");
        };
    }

    private CommandRunnable getRunnableGMOthers() {
        return (sender, player, parsedVariables) -> {
            int number = (Integer) parsedVariables[0];
            Player target = (Player) parsedVariables[1];
            target.setGameMode(GameMode.getByValue(number));
            sender.sendMessage("§cDu hast einen GM geändert!");
        };
    }

    @Override
    public String getName() {
        return "gm";
    }

    @Override
    public String getDescription() {
        return "Setzt den GameMode eines Spielers!";
    }

    @Override
    public boolean requiresPermission() {
        return true;
    }

    @Override
    public List<String> getAliases() {
        return Arrays.asList("g", "j");
    }

    @Override
    public boolean onlyForPlayer() {
        return false;
    }
}
