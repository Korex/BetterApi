package net.korex.betterapi.inventory;

import net.korex.betterapi.BetterApi;
import net.korex.betterapi.betteritem.BetterItem;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.inventory.InventoryType;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public abstract class BetterInventory {

    private String id;
    private BetterApi betterApi;
    private BetterInventoryConfig betterInventoryConfig;
    private List<BetterInventoryItem> contents;
    private HashMap<Integer, Integer> rulesFreeZones;
    private Inventory inventory;

    public BetterInventory(BetterApi betterApi, String id) {
        this.contents = new ArrayList<>();
        this.id = id;
        this.betterApi = betterApi;
        this.betterInventoryConfig = new BetterInventoryConfig(betterApi, this.id);

        if (getSize() < 9) {
            this.inventory = Bukkit.createInventory(null, InventoryType.ANVIL, this.getTitle());
        } else {
            this.inventory = Bukkit.createInventory(null, this.getSize(), this.getTitle());
        }
    }

    public void open(Player player) {
        this.setup();
        this.registerEvents();
        player.closeInventory();
        this.paint();
        player.openInventory(inventory);
        BetterInventoryManager.put(player, this);
    }

    public void paint() {
        this.inventory.clear();
        ItemStack filler = getFiller().getItem();
        for (int i = 0; i < getSize(); i++) {
            if (this.isInRulesFreeZone(i)) {
                continue;
            }
            if (this.inventory.getItem(i) != null) {
                continue;
            }
            inventory.setItem(i, filler);
        }

        for (BetterInventoryItem content : this.contents) {
            this.inventory.setItem(content.getSlot(), content.getBetterItem().getItem());
        }
    }

    public boolean canClose(){
        return true;
    }

    public abstract int getSize();

    public String getTitle() {
        String defaultString = "&8[&3" + this.id + "&8]";
        return this.betterInventoryConfig.getTitle(defaultString);
    }

    public abstract void setup();

    public abstract void registerEvents();

    public boolean isInRulesFreeZone(int slot) {
        if (this.rulesFreeZones != null) {
            for (Integer integer : this.rulesFreeZones.keySet()) {
                Integer to = this.rulesFreeZones.get(integer);
                if (integer <= slot && slot <= to) {
                    return true;
                }
            }

        }
        return false;
    }

    public void fillItem(int slot, String key, BetterItem defaultItem) {
        if (this.betterInventoryConfig != null) {
            defaultItem = this.betterInventoryConfig.getItem(key, defaultItem);
        }
        this.contents.add(new BetterInventoryItem(slot, key, defaultItem));
    }

    public void fillNonConfigurableItem(int slot, BetterItem item) {
        this.contents.add(new BetterInventoryItem(slot, null, item));
    }

    public void registerListener(int slot, BetterInventoryItem.BetterInventoryItemClickEvent clickEvent) {
        for (BetterInventoryItem content : this.contents) {
            if (content.getSlot() == slot) {
                content.setBetterInventoryItemClickEvent(clickEvent);
                return;
            }
        }
    }

    public BetterItem getFiller() {
        BetterItem defaultItem = new BetterItem(Material.AIR);

        return this.betterInventoryConfig.getItem("FILLER", defaultItem);
    }

    public BetterApi getBetterApi() {
        return betterApi;
    }

    public List<BetterInventoryItem> getContents() {
        return contents;
    }

    public void setRulesFreeZones(HashMap<Integer, Integer> rulesFreeZones) {
        this.rulesFreeZones = rulesFreeZones;
    }

    public Inventory getInventory() {
        return inventory;
    }

    public BetterInventoryConfig getBetterInventoryConfig() {
        return betterInventoryConfig;
    }

    public String getId() {
        return id;
    }

    public void setContents(List<BetterInventoryItem> contents) {
        this.contents = contents;
    }

    public HashMap<Integer, Integer> getRulesFreeZones() {
        return rulesFreeZones;
    }
}
