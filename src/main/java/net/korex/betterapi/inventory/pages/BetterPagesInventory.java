package net.korex.betterapi.inventory.pages;

import net.korex.betterapi.BetterApi;
import net.korex.betterapi.betteritem.BetterItem;
import net.korex.betterapi.inventory.BetterInventory;
import net.korex.betterapi.inventory.BetterInventoryItem;
import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;

import java.util.List;

public abstract class BetterPagesInventory extends BetterInventory {

    private int page;

    public BetterPagesInventory(BetterApi betterApi, String id) {
        super(betterApi, id);
        this.page = 0;
    }

    @Override
    public int getSize() {
        return 54;
    }


    @Override
    public void setup() {
        this.refreshPage();
    }

    private void setSwitcher() {
        BetterItem previousItem = this.getBetterInventoryConfig().getItem("PREVIOUS_PAGE", new BetterItem(Material.PAPER).setName("§aPrevious Page [%1/%2]"));
        previousItem.setName(previousItem.getMeta().getDisplayName().replace("%1", String.valueOf(this.page + 1)).replace("%2", String.valueOf(this.pages().size())));

        BetterItem nextItem = this.getBetterInventoryConfig().getItem("NEXT_PAGE", new BetterItem(Material.PAPER).setName("§aNext Page [%1/%2]"));
        nextItem.setName(nextItem.getMeta().getDisplayName().replace("%1", String.valueOf(this.page + 1)).replace("%2", String.valueOf(this.pages().size())));

        this.getInventory().setItem(48, previousItem.getItem());
        this.getContents().add(new BetterInventoryItem(48, "PREVIOUS_PAGE", previousItem));

        this.getInventory().setItem(50, nextItem.getItem());
        this.getContents().add(new BetterInventoryItem(50, "NEXT_PAGE", nextItem));
    }

    @Override
    public void registerEvents() {
        this.registerListener(48, player -> {
            if (this.page == 0) {
                return;
            }
            this.page = this.page - 1;
            this.refreshPage();
        });
        this.registerListener(50, player -> {
            int pagesSize = this.pages().size();
            if (this.page == (pagesSize - 1)) {
                return;
            }
            this.page = this.page + 1;
            this.refreshPage();
        });
    }

    public abstract List<BetterPage> pages();

    public void refreshPage() {
        BetterInventory newPage = this.pages().get(this.page);
        if (newPage.getContents() == null || newPage.getContents().isEmpty()) {
            newPage.setup();
            newPage.registerEvents();
        }

        this.setRulesFreeZones(newPage.getRulesFreeZones());
        this.setContents(newPage.getContents());
        this.paint();
        this.setSwitcher();
        this.registerEvents();
    }

    public int getPage() {
        return page;
    }
}
