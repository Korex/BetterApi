package net.korex.betterapi.inventory.pages;

import net.korex.betterapi.inventory.BetterInventory;

public abstract class BetterPage extends BetterInventory {

    private BetterPagesInventory betterPagesInventory;

    public BetterPage(BetterPagesInventory betterPagesInventory) {
        super(betterPagesInventory.getBetterApi(), betterPagesInventory.getId());
        this.betterPagesInventory = betterPagesInventory;
    }

    @Override
    public int getSize() {
        return 54;
    }

}
