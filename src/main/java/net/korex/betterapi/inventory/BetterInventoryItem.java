package net.korex.betterapi.inventory;

import net.korex.betterapi.betteritem.BetterItem;
import org.bukkit.entity.Player;

public class BetterInventoryItem {

    private int slot;
    private String key;
    private BetterItem betterItem;
    private BetterInventoryItemClickEvent betterInventoryItemClickEvent;

    public BetterInventoryItem(int slot, String key, BetterItem betterItem) {
        this.slot = slot;
        this.key = key;
        this.betterItem = betterItem;
    }

    public void setBetterInventoryItemClickEvent(BetterInventoryItemClickEvent betterInventoryItemClickEvent) {
        this.betterInventoryItemClickEvent = betterInventoryItemClickEvent;
    }

    public int getSlot() {
        return slot;
    }

    public String getKey() {
        return key;
    }

    public BetterItem getBetterItem() {
        return betterItem;
    }

    public BetterInventoryItemClickEvent getBetterInventoryItemClickEvent() {
        return betterInventoryItemClickEvent;
    }

    public interface BetterInventoryItemClickEvent {
        void run(Player player);
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("BetterInventoryItem{");
        sb.append("slot=").append(slot);
        sb.append(", key='").append(key).append('\'');
        sb.append(", betterItem=").append(betterItem);
        sb.append(", betterInventoryItemClickEvent=").append(betterInventoryItemClickEvent);
        sb.append('}');
        return sb.toString();
    }
}
