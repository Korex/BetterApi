package net.korex.betterapi.betterconfig;

import net.korex.betterapi.bettersql.SqlManager;
import org.bukkit.plugin.java.JavaPlugin;

public class BetterSqlConfig extends BetterConfig {

    public BetterSqlConfig(JavaPlugin javaPlugin, String fileName) {
        super(javaPlugin, fileName);

        this.getCfg().addDefault("mysql.user", "root");
        this.getCfg().addDefault("mysql.password", "password");
        this.getCfg().addDefault("mysql.database", "private");
        this.getCfg().addDefault("mysql.host", "localhost");
        this.getCfg().addDefault("mysql.port", 3306);
        this.getCfg().options().copyDefaults(true);

        this.save();
    }

    public void setupSql() {
        String user = this.getCfg().getString("mysql.user");
        String password = this.getCfg().getString("mysql.password");
        String host = this.getCfg().getString("mysql.host");
        String database = this.getCfg().getString("mysql.database");
        int port = this.getCfg().getInt("mysql.port");
        SqlManager.getInstance().connect(host, user, password, database, port);
    }


}
