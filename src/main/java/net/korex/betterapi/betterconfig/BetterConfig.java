package net.korex.betterapi.betterconfig;

import net.korex.betterapi.betteritem.BetterItem;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.plugin.java.JavaPlugin;

import java.io.File;
import java.io.IOException;
import java.util.List;

public class BetterConfig {

    private File file;
    private YamlConfiguration cfg;
    private String fileName;
    private JavaPlugin javaPlugin;

    public BetterConfig(JavaPlugin javaPlugin, String fileName) {
        this.javaPlugin = javaPlugin;
        this.fileName = fileName;
        this.reload();
    }

    public void reload() {
        if (!this.javaPlugin.getDataFolder().exists()) {
            this.javaPlugin.getDataFolder().mkdirs();
        }
        this.file = new File(this.javaPlugin.getDataFolder(), fileName.toLowerCase() + ".yml");
        this.cfg = YamlConfiguration.loadConfiguration(this.file);
    }

    public YamlConfiguration getCfg() {
        return this.cfg;
    }

    public void save() {
        try {
            this.cfg.save(this.file);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void setSimpleItemStack(String path, BetterItem itemStack) {
        this.getCfg().set(path + ".material", itemStack.getRawItemStack().getType().toString());
        this.getCfg().set(path + ".amount", itemStack.getRawItemStack().getAmount());
        if (itemStack.getMeta() == null) {
            return;
        }
        this.getCfg().set(path + ".name", itemStack.getMeta().getDisplayName());
        this.getCfg().set(path + ".lore", itemStack.getMeta().getLore());
        this.getCfg().set(path + ".data", itemStack.getRawItemStack().getData().getData());
    }

    public BetterItem getSimpleItemStack(String path, BetterItem defaultItem) {
        String materialString = this.getCfg().getString(path + ".material");
        int amount = this.getCfg().getInt(path + ".amount");
        int data = this.getCfg().getInt(path + ".data");
        String name = this.getCfg().getString(path + ".name");
        List<String> lore = this.getCfg().getStringList(path + ".lore");

        if (materialString == null) {
            if (name == null || lore == null) {
                this.setSimpleItemStack(path, defaultItem);
                this.save();
                return defaultItem;
            }
        }

        Material material = Material.getMaterial(materialString);

        if (name != null) {
            name = ChatColor.translateAlternateColorCodes('&', name);
        }

        if (lore != null) {
            for (int i = 0; i < lore.size(); i++) {
                String s = lore.get(i);
                s = ChatColor.translateAlternateColorCodes('&', s);
                lore.set(i, s);
            }

        }


        if (material == null) {
            this.setSimpleItemStack(path, defaultItem);
            this.save();
            return defaultItem;
        }
        ItemStack itemStack = new ItemStack(material, amount, (short) 0, (byte) data);

        if (itemStack == null) {
            this.setSimpleItemStack(path, defaultItem);
            this.save();
            return defaultItem;
        }
        ItemMeta meta = itemStack.getItemMeta();
        if (name != null && meta != null) {
            meta.setDisplayName(name);
        }
        if (lore != null && meta != null) {
            meta.setLore(lore);
        }
        itemStack.setItemMeta(meta);
        return new BetterItem(itemStack);
    }

    public String toSimpleLocation(Location location) {
        return location.getWorld().getName() + ", " + location.getBlockX() + ", " + location.getBlockY() + ", " + location.getBlockZ() + ", " + location.getYaw() + ", " + location.getPitch();
    }

    public Location fromSimpleLocation(String locationString) {
        String[] split = locationString.replace(" ", "").split(",");
        if (split.length != 6) {
            return null;
        }
        String worldName = split[0];
        int x = Integer.parseInt(split[1]);
        int y = Integer.parseInt(split[2]);
        int z = Integer.parseInt(split[3]);
        float yaw = Float.valueOf(split[4]);
        float pitch = Float.valueOf(split[5]);
        return new Location(Bukkit.getWorld(worldName), x, y, z, yaw, pitch);
    }

}
