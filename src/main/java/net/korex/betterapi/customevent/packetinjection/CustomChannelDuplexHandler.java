package net.korex.betterapi.customevent.packetinjection;

import io.netty.channel.ChannelDuplexHandler;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.ChannelPromise;
import net.korex.betterapi.Main;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.Event;
import org.bukkit.plugin.SimplePluginManager;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

public class CustomChannelDuplexHandler extends ChannelDuplexHandler {

    private Player player;

    public CustomChannelDuplexHandler(Player player) {
        this.player = player;
    }

    @Override
    public void channelRead(ChannelHandlerContext channelHandlerContext, Object o) {
        final ServerPacketReceiveEvent event = new ServerPacketReceiveEvent(this.player, o);
        this.callEvent(event);
        final Object packet = event.getPacket();
        if (packet != null) {
            try {
                super.channelRead(channelHandlerContext, packet);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public void write(ChannelHandlerContext channelHandlerContext, Object o, ChannelPromise channelPromise) {
        final ServerPacketSendEvent event = new ServerPacketSendEvent(this.player, o);
        this.callEvent(event);
        final Object packet = event.getPacket();
        if (packet != null) {
            try {
                super.write(channelHandlerContext, packet, channelPromise);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    private void callEvent(Event event) {
        try {
            Method fireEventMethod = Bukkit.getPluginManager().getClass().getDeclaredMethod("fireEvent", Event.class);
            fireEventMethod.setAccessible(true);

            fireEventMethod.invoke(Bukkit.getPluginManager(), event);
        } catch (NoSuchMethodException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            e.printStackTrace();
        }
    }

}
