package net.korex.betterapi.customevent.packetinjection;

import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerCommandPreprocessEvent;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerQuitEvent;

public class PacketInjectionListener implements Listener {

    private PacketInjectionManager manager;

    public PacketInjectionListener(PacketInjectionManager manager) {
        this.manager = manager;
    }

    @EventHandler
    public void onJoin(PlayerJoinEvent e) {
        this.manager.injectPlayer(e.getPlayer());
    }

    @EventHandler
    public void onLeave(PlayerQuitEvent e) {
        this.manager.uninjectPlayer(e.getPlayer());
    }

}
