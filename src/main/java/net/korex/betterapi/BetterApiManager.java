package net.korex.betterapi;

import java.util.ArrayList;
import java.util.List;

public class BetterApiManager {

    private static List<BetterApi> instances = new ArrayList<>();

    public static BetterApi getInstance(String name) {
        for (BetterApi instance : instances) {
            if (instance.getJavaPlugin().getName().equals(name)) {
                return instance;
            }
        }
        return null;
    }

    public static void add(BetterApi instance) {
        instances.add(instance);
    }

    public static void afterStart() {
        boolean allStarted = true;
        for (BetterApi betterApi : instances) {
            if (!betterApi.isStarted()) {
                allStarted = false;
            }
        }
        if (allStarted) {
            for (BetterApi betterApi : instances) {
                betterApi.afterAllStarted();
            }
        }
    }

    public static List<BetterApi> getInstances() {
        return instances;
    }
}
