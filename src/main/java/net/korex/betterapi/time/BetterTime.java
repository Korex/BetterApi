package net.korex.betterapi.time;

public class BetterTime {

    public static String getPastTime(long one, long two) {
        long past = Math.max(one, two) - Math.min(one, two);

        if (past > (60 * 60 * 1000 * 24)) {
            double d = past;
            d = d / 24 / 60 / 60 / 1000;
            d = d * 10;
            d = Math.round(d);
            d = d / 10;

            return d + "d";
        } else if (past > (60 * 60 * 1000)) {
            double d = past;
            d = d / 60 / 60 / 1000;
            d = d * 10;
            d = Math.round(d);
            d = d / 10;

            return d + "h";
        } else if (past < (1000 * 60)) {
            return past / 1000 + "s";
        } else {
            return past / 60 / 1000 + "m";
        }
    }

}
