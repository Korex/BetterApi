package net.korex.betterapi.bettermanager;

import net.korex.betterapi.BetterApi;
import net.korex.betterapi.bettersql.AutoSqlTable;
import net.korex.betterapi.bettersql.annotations.SqlAutoIncrement;
import net.korex.betterapi.bettersql.annotations.SqlPrimary;
import org.bukkit.Bukkit;

import java.lang.annotation.Annotation;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.UUID;
import java.util.function.Consumer;

public class BetterManager<T> {

    private HashMap<UUID, List<T>> cache;
    private AutoSqlTable sqlTable;

    public BetterManager(BetterApi betterPlugin, String tableName, Class<?> dataClass, boolean registerListener) {
        this.containsField(dataClass, "id", SqlAutoIncrement.class, SqlPrimary.class);
        this.containsField(dataClass, "uuid");

        this.sqlTable = new AutoSqlTable(tableName, dataClass);
        this.sqlTable.create();

        this.cache = new HashMap<>();
        if (registerListener) {
            this.registerListener(betterPlugin);
        }
    }


    private void containsField(Class<?> dataClass, String name, Class<? extends Annotation>... annotations) {
        try {
            Field field = dataClass.getDeclaredField(name);

            if (field == null) {
                throw new RuntimeException("Data class " + dataClass.getName() + " has no field with " + name + " (look at upper/lowercase)!");
            }

            for (Class<? extends Annotation> annotation : annotations) {
                if (field.isAnnotationPresent(annotation)) {
                    continue;
                }

                throw new RuntimeException("Data class " + dataClass.getName() + " field " + name + " is missing with the following annotation: @" + annotation);
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    private void registerListener(BetterApi betterPlugin) {
        BetterManagerListener listener = new BetterManagerListener(this);
        betterPlugin.getJavaPlugin().getServer().getPluginManager().registerEvents(listener, betterPlugin.getJavaPlugin());
    }

    public void loadAll() {
        Bukkit.getOnlinePlayers().forEach((player) -> {
            this.load(player.getUniqueId(), null);
        });
    }

    public boolean isLoaded(UUID uuid) {
        return this.cache.containsKey(uuid);
    }

    public void putToCache(UUID uuid, List<T> t) {
        this.cache.put(uuid, t);
    }

    public List<T> getFromCache(UUID uuid) {
        if (!this.cache.containsKey(uuid)) {
            return null;
        }
        return this.cache.get(uuid);
    }

    public void removeFromCache(UUID uuid) {
        this.cache.remove(uuid);
    }

    public void load(UUID uuid, Consumer<Void> after) {
        this.sqlTable.get("uuid=" + uuid.toString()).async(sqlResultSet -> {
            if (sqlResultSet.size() == 0) {
                if (after != null) {
                    after.accept(null);
                }
                return;
            }

            List<T> data = new ArrayList<>();
            for (int i = 0; i < sqlResultSet.size(); i++) {
                Object dataObject = sqlResultSet.get(i);
                data.add((T) dataObject);
            }

            if (data.size() == 0) {
                return;
            }

            this.cache.put(uuid, data);
            if (after != null) {
                after.accept(null);
            }

        });
    }

    public void writeAll() {
        Bukkit.getOnlinePlayers().forEach(player -> this.write(player.getUniqueId()));
    }

    public void write(UUID uuid) {
        List<T> list = this.getFromCache(uuid);
        if (list == null || list.size() == 0) {
            return;
        }

        for (T t : this.getFromCache(uuid)) {
            this.sqlTable.put(t).async(null);
        }
    }

    public AutoSqlTable getSqlTable() {
        return sqlTable;
    }
}
